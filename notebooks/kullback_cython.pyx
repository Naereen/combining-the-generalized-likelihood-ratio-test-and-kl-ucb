# -*- coding: utf-8 -*-
""" Kullback-Leibler divergence functions for Bernoulli and Gaussian distributions. Optimized version that should be compiled using Cython (http://docs.cython.org/).


.. note::

    The Python version is slower. The Cython version has the advantage of providing docstrings, and optional arguments, and is only 2 times slower than a pure C version, while being 3 times faster than a naive Python version.

    >>> import kullback; import kullback_cython  # fake, just for illustration

    >>> r = np.random.random
    >>> %timeit kullback_c.klBern(r(), r())
    1.78 µs ± 213 ns per loop (mean ± std. dev. of 7 runs, 1000000 loops each)
    >>> %timeit kullback_cython.klBern(r(), r())
    707 ns ± 151 ns per loop (mean ± std. dev. of 7 runs, 1000000 loops each)

    >>> r = np.random.randn
    >>> %timeit kullback_c.klGauss(r(), r())
    888 ns ± 49.1 ns per loop (mean ± std. dev. of 7 runs, 1000000 loops each)
    >>> %timeit kullback_cython.klGauss(r(), r())
    719 ns ± 6.08 ns per loop (mean ± std. dev. of 7 runs, 1000000 loops each)


.. warning::

    This extension should be used with the ``setup.py`` script, by running::

        $ python setup.py build_ext --inplace

    You can also use [pyximport](http://docs.cython.org/en/latest/src/tutorial/cython_tutorial.html#pyximport-cython-compilation-for-developers) to import the ``kullback_cython`` module transparently:

    >>> import pyximport; pyximport.install()  # instantaneous  # doctest: +ELLIPSIS
    (None, <pyximport.pyximport.PyxImporter at 0x...>)
    >>> import kullback_cython as kullback     # takes about two seconds
    >>> # then use kullback.klBern or others, as if they came from the pure Python version!
"""
from __future__ import division, print_function  # Python 2 compatibility

__author__ = "Olivier Cappé, Aurélien Garivier, Lilian Besson"
__version__ = "0.9"

from libc.math cimport log, sqrt, exp


cdef float eps = 1e-9  #: Threshold value: everything   >>>1] is truncated to [eps, 1 - eps]


# --- Simple Kullback-Leibler divergence for known distributions


def klBern(float x, float y) -> float:
    r""" Kullback-Leibler divergence for Bernoulli distributions. https://en.wikipedia.org/wiki/Bernoulli_distribution#Kullback.E2.80.93Leibler_divergence

    .. math:: \mathrm{KL}(\mathcal{B}(x), \mathcal{B}(y)) = x \log(\frac{x}{y}) + (1-x) \log(\frac{1-x}{1-y}).

    >>> klBern(0.5, 0.5)
    0.0
    >>> klBern(0.1, 0.9)  # doctest: +ELLIPSIS
    1.757779...
    >>> klBern(0.9, 0.1)  # And this KL is symmetric  # doctest: +ELLIPSIS
    1.757779...
    >>> klBern(0.4, 0.5)  # doctest: +ELLIPSIS
    0.020135...
    >>> klBern(0.01, 0.99)  # doctest: +ELLIPSIS
    4.503217...

    - Special values:

    >>> klBern(0, 1)  # Should be +inf, but 0 --> eps, 1 --> 1 - eps  # doctest: +ELLIPSIS
    34.539575...
    """
    x = min(max(x, eps), 1 - eps)
    y = min(max(y, eps), 1 - eps)
    return x * log(x / y) + (1 - x) * log((1 - x) / (1 - y))


def klGauss(float x, float y, float sig2x=0.25) -> float:
    r""" Kullback-Leibler divergence for Gaussian distributions of means ``x`` and ``y`` and variances ``sig2x=sig2y``, :math:`\nu_1 = \mathcal{N}(x, \sigma_x^2)` and :math:`\nu_2 = \mathcal{N}(y, \sigma_x^2)`:

    .. math:: \mathrm{KL}(\nu_1, \nu_2) = \frac{(x - y)^2}{2 \sigma_y^2} + \frac{1}{2}\left( \frac{\sigma_x^2}{\sigma_y^2} - 1 \log\left(\frac{\sigma_x^2}{\sigma_y^2}\right) \right).

    See https://en.wikipedia.org/wiki/Normal_distribution#Other_properties

    - By default, sig2y is assumed to be sig2x (same variance).

    >>> klGauss(3, 3)
    0.0
    >>> klGauss(3, 6)
    18.0
    >>> klGauss(1, 2)
    2.0
    >>> klGauss(2, 1)  # And this KL is symmetric
    2.0
    >>> klGauss(4, 2)
    8.0
    >>> klGauss(6, 8)
    8.0

    - x, y can be negative:

    >>> klGauss(-3, 2)
    50.0
    >>> klGauss(3, -2)
    50.0
    >>> klGauss(-3, -2)
    2.0
    >>> klGauss(3, 2)
    2.0

    - With other values for `sig2x`:

    >>> klGauss(3, 3, sig2x=10)
    0.0
    >>> klGauss(3, 6, sig2x=10)
    0.45
    >>> klGauss(1, 2, sig2x=10)
    0.05
    >>> klGauss(2, 1, sig2x=10)  # And this KL is symmetric
    0.05
    >>> klGauss(4, 2, sig2x=10)
    0.2
    >>> klGauss(6, 8, sig2x=10)
    0.2

    .. warning:: Using :class:`Policies.kl` (and variants) with :func:`klGauss` is equivalent to use :class:`Policies.UCB`, so prefer the simpler version.
    """
    return (x - y) ** 2 / (2. * sig2x)
